﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Collections;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Array
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            Collection obj = new Collection();
            int[] r=obj.CollectionArray();
            for(int i=0;i<r.Length;i++)
            {
                Listbox1.Items.Add(r[i].ToString());
                
            }
            List<String> objlist = obj.ListCollectionArray();
            for(int i=0;i<objlist.Count;i++)
            {
                Listbox1.Items.Add(objlist[i].ToString());
                
            }
            Hashtable hlist = obj.CollectionHashTable();
            for(int i=1;i<hlist.Count+1;i++)
            {
                Listbox1.Items.Add((string)hlist[i]);
            }
            
            Dictionary<int, String> dlist = obj.CollectionDictionary();
            for(int i=1;i<dlist.Count+1;i++)
            {
                
                Listbox1.Items.Add((string)dlist[i]);
            }
                    }
    }
}
