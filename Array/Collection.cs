﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array
{
    class Collection
    {
        int a=0;
        int b=0;
        int[] arr;
        public int[] CollectionArray()
        {
            arr = new int[2];
            arr[0] = 10;
            arr[1] = 20;
            return arr;
        }
        List<String> lst;
        public List<String> ListCollectionArray()
        {
            lst = new List<string>();
            lst.Add("Priyanka");
            lst.Add("Lipika");
            lst.Add("Smita");
            lst.Add("Sagar");
            return lst;
        }
        Hashtable htable;
        public Hashtable CollectionHashTable()
        {
            htable = new Hashtable();
            htable.Add(1, "Priyanka");
            htable.Add(2, "Lipika");
            htable.Add(3, "Smita");
            htable.Add(4, "Sagar");
            return htable;
        }
        Dictionary<int, String> objCollD;
        public Dictionary<int,String> CollectionDictionary()
        {
            objCollD = new Dictionary<int, string>();
            objCollD.Add(1, "Priyanka");
            objCollD.Add(2, "Sagar");
            objCollD.Add(3, "Lipika");
            objCollD.Add(4, "Smita");
            return objCollD;
        }
    }
}
